<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    public function produtos()
    {
        if(Auth::check() == true) {
            return view('produto.list');
        }
    return redirect() ->route ( 'produtos.login');

    }
    public function showLoginForm()
    {
        return view('produto.formLogin');
    }

    public function login(Request $request)
    {
        if(!filter_var($request->email, FILTER_VALIDATE_EMAIL)){
            return redirect()->back()->withInput()->withErrors(['O email informado não é valido!']);
        }
        var_dump($request->all());

        $credentials = [
            'email'=> $request->email,
            'password'=> $request->password
        ];

        if(Auth::attempt($credentials)){
            return redirect()->route('produtos');
        }
        return redirect()->back()->withInput()->withErrors(['Os dados informados não conferem']);
    }

    public function logout()
    {
        Auth::logout();
        return redirect()-> route('produtos');
    }
}
