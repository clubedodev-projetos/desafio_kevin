<?php
namespace App\Http\Controllers;
use App\Produto;
use Illuminate\Http\Request;
use Validator;
use Redirect;
use PDF;
class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['produtos'] = Produto::orderBy('id','desc')->paginate(10);
        return view('produto.list',$data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('produto.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nome' => 'required',
            'preco' => 'required',
            'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'descricao' => 'required',
        ]);
        if ($files = $request->file('foto')) {
            $destinationPath = 'public/foto/'; // upload path
            $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profileImage);
            $insert['foto'] = "$profileImage";
        }
        $insert['nome'] = $request->get('nome');
        $insert['preco'] = $request->get('preco');
        $insert['descricao'] = $request->get('descricao');
        Produto::insert($request->only('nome','preco', 'descricao', 'foto'));
        return Redirect::to('produtos')->with('sucesso','Otimo! Produto criado corretamente.');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $where = array('id' => $id);
        $data['preco'] = Produto::where($where)->first();
        return view('produto.edit', $data);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nome' => 'required',
            'preco' => 'required',
            'descricao' => 'required',
        ]);
        $update = ['nome' => $request->nome, 'descricao' => $request->descricao];
        if ($files = $request->file('foto')) {
            $destinationPath = 'public/foto/'; // upload path
            $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profileImage);
            $update['foto'] = "$profileImage";
        }
        $update['nome'] = $request->get('nome');
        $update['preco'] = $request->get('preco');
        $update['descricao'] = $request->get('descricao');
        Produto::where('id',$id)->update($update);
        return Redirect::to('produtos')->with('sucesso','Otimo! Produto atualizado corretamente');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Produto::where('id',$id)->delete();
        return Redirect::to('produtos')->with('sucesso','Produto deletado corretamente');
    }
}
