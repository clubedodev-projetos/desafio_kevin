@extends('produto.layout')
@section('content')
    <h2 style="margin-top: 12px;" class="text-center">Adicionar Produto</h2>
    <br>
    <form action="{{ route('produtos.store') }}" method="POST" enctype="multipart/form-data" name="add_produto"{{ csrf_field() }}
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <strong>Nome</strong>
                    <input type="text" name="nome" class="form-control" placeholder="Nome">
                    <span class="text-danger">{{ $errors->first('nome') }}</span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <strong>Preco</strong>
                    <input type="float" name="preco" class="form-control" placeholder="Preco">
                    <span class="text-danger">{{ $errors->first('preco') }}</span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <strong>Descrição</strong>
                    <textarea class="form-control" col="4" name="descricao" placeholder="Descricao"></textarea>
                    <span class="text-danger">{{ $errors->first('descricao') }}</span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <strong>Foto do produto </strong>
                    <input type="file" name="foto" class="form-control" placeholder="">
                    <span class="text-danger">{{ $errors->first('foto') }}</span>
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary">Cadastrar</button>
            </div>
        </div>
    </form>
@endsection
