@extends('produto.layout')
@section('content')
    <h2 style="margin-top: 12px;" class="text-center">Editar Produto</h2>
    <br>
    <form action="{{ route('produtos.update', $produto_info->id) }}" method="{{ csrf_field() }}">
        @method('PATCH')
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <strong>Nome</strong>
                    <input type="text" name="nome" class="form-control" placeholder="Nome" value="{{ $produto_info->nome}}">
                    <span class="text-danger">{{ $errors->first('nome') }}</span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <strong>Preco</strong>
                    <input type="text" name="preco" class="form-control" placeholder="Preco" value="{{ $produto_info->preco }}">
                    <span class="text-danger">{{ $errors->first('preco') }}</span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <strong>Description</strong>
                    <textarea class="form-control" col="4" name="descricao" placeholder="Enter Description" >{{ $produto_info->descricao }}</textarea>
                    <span class="text-danger">{{ $errors->first('descricao') }}</span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <strong>Foto do Produto</strong>
                    @if($produto_info->foto)
                        <img id="original" src="{{ url('public/foto/'.$produto_info->foto) }}" height="70" width="70">
                    @endif
                    <input type="text" name="foto" class="form-control" placeholder="" value="{{ $produto_info->foto }}">
                    <span class="text-danger">{{ $errors->first('foto') }}</span>
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary">Enviar</button>
            </div>
        </div>
    </form>
@endsection
