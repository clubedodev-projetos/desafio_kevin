<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <style>
        html,
        body{
            height: 100%;
        }
        body{
            display: -ms-flexbox;
            display: flex;
            -ms-flex-align: center;
            align-items: center;
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #ffffff;
        }

        .form-signin{
            width: 100%;
            max-width: 330px;
            padding: 15px;
            margin: auto;
        }
        .form-signin .checkbox{
            font-weight: 400;
        }
        .form-signin .form-control{
            position: relative;
            box-sizing: border-box;
            height: auto;
            padding: 10px;
            font-size: 16px;
        }
        .form-signin .form-control:focus{
            z-index: 2;
        }
        .form-signin input[type="email"]{
            margin-bottom: -20px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
        .form-signin input[type="password"]{
            margin-bottom: 15px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
        img{
            max-width: 50%;
            margin-bottom: 30px;
            display: block;
            margin-left: auto;
            margin-right: auto;
        }
    </style>

</head>
<body>

<form class="form-signin" method="post" action="{{route('produtos.login.do')}}">
    @csrf
    <img src="https://lh3.googleusercontent.com/-Ia95EEt1bJS3jdznRQetWhpn3uuTQxSqdJ6JJjp7TNN6xjkBD-aue0ZGnA1amJaojz_j98=s85" alt="">

    @if($errors->all())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{$error}}
            </div>
        @endforeach

    @endif

    <label for="email" class="sr-only">Endereço de E-mail</label><br />
    <input type="text" name="email" id="email" class="form-control" placeholder="E-mail" required><br />

    <label for="password" class="sr-only">Senha</label><br />
    <input type="password" name="password" id="password" class="form-control" placeholder="Senha" required><br />
    <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
</form>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
