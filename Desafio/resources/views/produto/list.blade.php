@extends('produto.layout')
@section('content')
    <a href="{{ route('produtos.create') }}" class="btn btn-success mb-2">Adicionar</a>
    <br>
    <div class="row">
        <div class="col-12">
            <table class="table table-bordered" id="laravel_crud">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Nome</th>
                    <th>Preco</th>
                    <th>Descricao</th>
                    <th>Criado em</th>
                    <td colspan="2">Action</td>
                </tr>
                </thead>
                <tbody>
                @foreach($produtos as $produto)
                    <tr>
                        <td>{{ $produto->id }}</td>
                        <td>{{ $produto->nome }}</td>
                        <td>{{ $produto->preco }}</td>
                        <td>{{ $produto->descricao }}</td>
                        <td>{{ date('Y-m-d', strtotime($produto->created_at)) }}</td>
                        <td><a href="{{ route('produtos.edit',$produto->id)}}" class="btn btn-primary">Editar</a></td>
                        <td>
                            <form action="{{ route('produtos.destroy', $produto->id)}}" method="post">
                                {{ csrf_field() }}
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $produtos->links() !!}
        </div>
    </div>
@endsection
