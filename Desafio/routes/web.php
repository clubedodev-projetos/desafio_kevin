<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home/logout', 'LoginController@logout')->name('logout');
//Route::get('/produtos', 'AuthController@produtos')->name('produtos');

//Route::get('produto/login','AuthController@showLoginForm')->name('produtos.login');
//Route::get('produto/logout','AuthController@showLoginForm')->name('produtos.logout');

//Route::post('/produtos/login/do','AuthController@login')->name('produtos.login.do');

Route::resource('produtos', 'ProdutoController');
Route::get('produtos', 'ProdutoController@create')->name('produtos.create');

/*Auth::routes();

Route::get('/produtos/novo', 'ProdutosController@create')->name('produto');*/
